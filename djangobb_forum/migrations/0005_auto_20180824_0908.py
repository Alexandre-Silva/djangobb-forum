# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-08-24 08:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('djangobb_forum', '0004_auto_20180824_0851'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachment',
            name='file',
            field=models.FileField(max_length=255, null=True, upload_to=b'djangobb_forum/attachments', verbose_name='File'),
        ),
    ]
